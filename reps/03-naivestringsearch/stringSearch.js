function stringSearch(str, pattern) {
  let matchCount = 0;

  for (let i = 0; i < str.length; i++) {
    for (let j = 0; j < pattern.length; j++) {
      if (str[i] !== pattern[j]) break;
      else {
        i++;
        if (j === pattern.length - 1) matchCount++;
      }
    }
  }
  return matchCount;
}

console.log(stringSearch('omgwowomgz', 'omg'));
