def swap(arr, idx1, idx2):
    temp = arr[idx1]
    arr[idx1] = arr[idx2]
    arr[idx2] = temp


def selectionSort(arr):
    for i in range(0, len(arr)):
        lowest = i
       
        for j in range(i + 1, len(arr)):
            if arr[j] < arr[lowest]:
                lowest = j
        swap(arr, i, lowest)
    return arr


print(selectionSort([99, -14, 6, 0, 727, -1462]))