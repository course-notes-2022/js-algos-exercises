# Bubble Sort

```js
function swap(arr, idx1, idx2) {
  let temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let noSwaps = true;
    for (let j = 0; j <= i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        noSwaps = false;
      }
    }
    if(noSwaps) break;
  }
  return arr;
}


//                       0   1  2  3
console.log(bubbleSort([99, -3, 7, 145]))'
```

| i   | j   | j+1 | arr[j] | arr[j+1] | arr[j] > arr[j+1]? | swap? | arr           |
| --- | --- | --- | ------ | -------- | ------------------ | ----- | ------------- |
| 3   | 0   | 1   | 99     | -3       | true               | yes   | [-3,99,7,145] |
| 3   | 1   | 2   | 99     | 7        | true               | yes   | [-3,7,99,145] |
| 3   | 2   | 3   | 99     | 145      | false              | no    | [-3,7,99,145] |

## Explanation

With **Bubble Sort**, we use nested loops to compare each item in the array with
the item **after** it. If the first item is **less than** the next, we swap them
**right away**. We then move on to the next index and repeat the
comparison/swapping until the array is sorted.
