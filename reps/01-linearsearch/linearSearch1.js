function linearSearch(arr, val) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === val) return i;
  }
  return -1;
}

console.log(linearSearch([1, 5, 22, -3, 459, 97654321], 459)); // // 4
console.log(linearSearch([1, 5, 22, -3, 459, 97654321], 'foo')); // -1
