function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let hasNoSwaps = true;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j + 1] < arr[j]) {
        swap(arr, j, j + 1);
        hasNoSwaps = false;
      }
    }
    if (hasNoSwaps) break;
  }
  return arr;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let leastIdx = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[leastIdx]) {
        leastIdx = j;
      }
    }
    swap(arr, i, leastIdx);
  }
  return arr;
}

function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentVal = arr[i];
    let j;
    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}

const arr = [-99, 4, 1742, 0 - 131, 19];

console.log(bubbleSort(arr));
console.log(selectionSort(arr));
console.log(insertionSort(arr));
