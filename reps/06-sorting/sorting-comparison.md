# Sorting Comparison

```js
const arr = [99, -4, 0];
```

## `selectionSort`

```js
function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let smallest = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j + 1] < arr[smallest]) {
        arr[j + 1] = arr[smallest];
        smallest = j;
      }
    }
  }
  return arr;
}
```

| i   | smallest | j   | arr[j] | j+1 | arr[j+1] | arr[smallest] | arr[j+1] < arr[smallest]? | arr[j+1] | arr        | smallest |
| --- | -------- | --- | ------ | --- | -------- | ------------- | ------------------------- | -------- | ---------- | -------- |
| 0   | 0        | 1   | -4     | 2   | 0        | 99            | true                      | 99       | [99,-4,99] | 1        |
