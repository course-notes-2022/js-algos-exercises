function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let mindex = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[mindex]) {
        mindex = j;
      }
    }
    swap(arr, i, mindex);
  }
  return arr;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let noSwaps = true;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j + 1] < arr[j]) {
        swap(arr, j, j + 1);
        noSwaps = false;
      }
    }
    if (noSwaps) break;
  }
  return arr;
}

function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentVal = arr[i];
    let j;
    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}

// [9, 10, 11, -11, 0, 248, -2.5]

console.log(selectionSort([9, 10, 11, -11, 0, 248, -2.5]));
console.log(bubbleSort([9, 10, 11, -11, 0, 248, -2.5]));
console.log(insertionSort([9, 10, 11, -11, 0, 248, -2.5]));
