function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let smallestIdx = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[smallestIdx]) {
        smallestIdx = j;
      }
    }
    swap(arr, i, smallestIdx);
  }
  return arr;
}

function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentValue = arr[i];
    let j;
    for (j = i - 1; j >= 0 && arr[j] > currentValue; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentValue;
  }
  return arr;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let hasNoSwaps = true;
    for (j = 0; j < arr.length; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        hasNoSwaps = false;
      }
    }
    if (hasNoSwaps) break;
  }
  return arr;
}
// [270, 18, -3, 0.568, 4, 0]

console.log(bubbleSort([270, 18, -3, 0.568, 4, 0]));
console.log(insertionSort([270, 18, -3, 0.568, 4, 0]));
console.log(selectionSort([270, 18, -3, 0.568, 4, 0]));
