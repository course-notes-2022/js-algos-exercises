function swap(arr, index1, index2) {
  const holder = arr[index1];
  arr[index1] = arr[index2];
  arr[index2] = holder;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let hasNoSwaps = true;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        hasNoSwaps = false;
      }
    }
    if (hasNoSwaps) break;
  }
  return arr;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let minIdx = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[minIdx]) {
        minIdx = j;
      }
    }
    swap(arr, i, minIdx);
  }
  return arr;
}

function insertionSort(arr) {
  for (i = 1; i < arr.length; i++) {
    let currentVal = arr[i];
    let j;

    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}
// [18, -23, 0, 642, 7, -9.02]

console.log(bubbleSort([18, -23, 0, 642, 7, -9.02]));
console.log(selectionSort([18, -23, 0, 642, 7, -9.02]));
console.log(insertionSort([18, -23, 0, 642, 7, -9.02]));
