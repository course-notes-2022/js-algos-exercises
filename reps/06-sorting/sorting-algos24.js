function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let hasNoSwaps = 0;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j + 1] < arr[j]) {
        swap(arr, j, j + 1);
        hasNoSwaps = false;
      }
    }
    if (hasNoSwaps) break;
  }
  return arr;
}

function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentVal = arr[i];
    let j;
    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let mindex = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[mindex]) {
        mindex = j;
      }
    }
    swap(arr, i, mindex);
  }
  return arr;
}

function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

// [204, -9, 14, 0, 14.56, -7]

console.log(bubbleSort([204, -9, 14, 0, 14.56, -7]));
console.log(insertionSort([204, -9, 14, 0, 14.56, -7]));
console.log(selectionSort([204, -9, 14, 0, 14.56, -7]));
