function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let hasNoSwaps = true;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        hasNoSwaps = false;
      }
    }
    if (hasNoSwaps) break;
  }
  return arr;
}

function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentVal = arr[i];
    let j;
    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let smallestIdx = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[smallestIdx]) {
        swap(arr, j, smallestIdx);
      }
    }
  }
  return arr;
}

// [-197, 4, 14, 0, 63, -2.78484848]
console.log(bubbleSort([-197, 4, 14, 0, 63, -2.78484848]));
console.log(insertionSort([-197, 4, 14, 0, 63, -2.78484848]));
console.log(selectionSort([-197, 4, 14, 0, 63, -2.78484848]));
