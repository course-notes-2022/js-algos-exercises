function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let minVal = arr[i];
    let j;
    for (j = i - 1; j >= 0 && arr[j] > minVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = minVal;
  }
  return arr;
}

function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let minIdx = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[minIdx]) {
        minIdx = j;
      }
    }
    swap(arr, i, minIdx);
  }
  return arr;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let noSwaps = true;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        noSwaps = false;
      }
    }
    if (noSwaps) break;
  }
  return arr;
}
// [27, 942, -7, 6, 0, -3.762]

console.log(insertionSort([27, 942, -7, 6, 0, -3.762]));
console.log(selectionSort([27, 942, -7, 6, 0, -3.762]));
console.log(bubbleSort([27, 942, -7, 6, 0, -3.762]));
