function insertionSort(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentVal = arr[i];
    let j;

    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}

function swap(arr, idx1, idx2) {
  const temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let currentIdx = i;
    for (let j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[currentIdx]) {
        currentIdx = j;
      }
    }
    swap(arr, i, currentIdx);
  }
  return arr;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let hasNoSwaps = true;
    for (let j = 0; j < arr.length; j++) {
      if (arr[j + 1] < arr[j]) {
        swap(arr, j, j + 1);
      }
    }
    if (hasNoSwaps) break;
  }
  return arr;
}
const arr = [123, 0, -19234, 47, 600, 6];

console.log(selectionSort(arr));
console.log(insertionSort(arr));
console.log(bubbleSort(arr));
