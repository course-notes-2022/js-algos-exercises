def swap(my_list, idx1, idx2):
    temp = my_list[idx2]
    my_list[idx2] = my_list[idx1]
    my_list[idx1] = temp

def bubbleSort(my_list):
    for i in range(len(my_list), -1, -1):
        noSwaps = True
        # print(i)
        for j in range(0, i - 1):
            if my_list[j] > my_list[j+1]:
                swap(my_list, j, j+1)
                noSwaps = False
        if noSwaps:
            break

    return my_list

print(bubbleSort([-11, 22, 1437, 0, 16, 999, 2]))
