# Bubble Sort

Bubble sort works by **looping** over each item in an array, and \*\*comparing
it

![bubble sort](./gifs/bubble-sort.gif)

## My Implementation

```js
function swap(arr, idx1, idx2) {
  let temp = arr[idx2];
  arr[idx2] = arr[idx1];
  arr[idx1] = temp;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i >= 0; i--) {
    let noSwaps = true;
    for (let j = 0; j <= i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        noSwaps = false;
      }
    }
    if (noSwaps) break;
  }
  return arr;
}
```

```js
//      0   1   2     3  4   5    6
arr = [-11, 22, 1437, 0, 16, 999, 2]

arr.length - 1 = 6
```

| `i` | `j` | `j+1` | arr[j] | arr[j+1] | arr[j] > arr[j+1]? | swap? | `noSwaps` | arr                            |
| --- | --- | ----- | ------ | -------- | ------------------ | ----- | --------- | ------------------------------ |
| 6   | 0   | 1     | -11    | 22       | false              | no    | false     | [-11, 22, 1437, 0, 16, 999, 2] |
| 6   | 1   | 2     | 22     | 1437     | false              | no    | false     | [-11, 22, 1437, 0, 16, 999, 2] |
| 6   | 2   | 3     | 1437   | 0        | true               | yes   | false     | [-11, 22, 0, 1437, 16, 999, 2] |
| 6   | 3   | 4     | 1437   | 16       | true               | yes   | false     | [-11, 22, 0, 16, 1437, 999, 2] |
| 6   | 4   | 5     | 1437   | 999      | true               | yes   | false     | [-11, 22, 0, 16, 999, 1437, 2] |
| 6   | 5   | 6     | 1437   | 2        | true               | yes   | false     | [-11, 22, 0, 16, 999, 2, 1437] |
| 5   | 0   | 1     | -11    | 22       | false              | no    | false     | [-11, 22, 0, 16, 999, 2, 1437] |
| 5   | 1   | 2     | 22     | 0        | true               | yes   | false     | [-11, 0, 22, 16, 999, 2, 1437] |
| 5   | 2   | 3     | 22     | 16       | true               | yes   | false     | [-11, 0, 16, 22, 999. 2, 1437] |
| 5   | 3   | 4     | 22     | 999      | false              | no    | false     | [-11, 0, 16, 22, 999, 2, 1437] |
| 5   | 4   | 3     | 999    | 2        | true               | yes   | false     | [-11, 0, 16, 22, 2, 999, 1437] |
| 4   | 0   | 1     | -11    | 0        | false              | no    | false     | [-11, 0, 16, 22, 2, 999, 1437] |
| 4   | 1   | 2     | 0      | 16       | false              | no    | false     | [-11, 0, 16, 22, 2, 999, 1437] |
| 4   | 2   | 3     | 16     | 22       | false              | no    | false     | [-11, 0, 16, 22, 2, 999, 1437] |
| 4   | 3   | 4     | 22     | 2        | true               | yes   | false     | [-11, 0, 16, 2, 22, 999, 1437] |
| 3   | 0   | 1     | -11    | 0        | false              | no    | false     | [-11, 0, 16, 2, 22, 999, 1437] |
| 3   | 1   | 2     | 0      | 16       | false              | no    | false     | [-11, 0, 16, 2, 22, 999, 1437] |
| 3   | 2   | 3     | 16     | 2        | true               | yes   | false     | [-11, 0, 2, 16, 22, 999, 1437] |
| 3   | 3   | 4     | 16     | 22       | false              | no    | false     | [-11, 0, 2, 16, 22, 999, 1437] |
| 2   | 0   | 1     | -11    | 0        | false              | no    | true      | [-11, 0, 2, 16, 22, 999, 1437] |
