/*
    Bubble Sort

    Create a function called "bubbleSort". Start looping
    from the END of the array with a variable
    called "i" towards the beginning.

    Start an inner loop with a variable called "j" from
    the beginning until i - 1

    If arr[j] is greater than arr[j+1], swap those two
    values.

    Return the sorted array.
*/

function swap(arr, idx1, idx2) {
  let temp = arr[idx2];
  arr[idx2] = arr[idx1];
  arr[idx1] = temp;
}

function bubbleSort(arr) {
  for (let i = arr.length - 1; i > -1; i -= 1) {
    let noSwaps = true;
    for (let j = 0; j <= i - 1; j++) {
      if (arr[j] > arr[j + 1]) {
        swap(arr, j, j + 1);
        noSwaps = false;
      }
    }
    if (noSwaps) break;
  }
  return arr;
}

console.log(bubbleSort([-11, 22, 1437, 0, 16, 999, 2]));
