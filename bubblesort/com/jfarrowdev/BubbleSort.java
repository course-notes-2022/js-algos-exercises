package bubblesort.com.jfarrowdev;

import java.util.Arrays;

public class BubbleSort {
    
    public static void main(String[] args) {
        int[] arr = { -11, 22, 1437, 0, 16, 999, 2 };
        int[] result = bubbleSort(arr);
        
        System.out.println(Arrays.toString(result));
    }

    public static void swap(int[] arr, int idx1, int idx2) {
        int temp = arr[idx1];
        arr[idx1] = arr[idx2];
        arr[idx2] = temp;
    }

    public static int[] bubbleSort(int[] arr) {
        for (int i = arr.length - 1; i >= 0; i -= 1) {
            boolean noSwaps = true;
            for (int j = 0; j <= i - 1; j += 1) {
                if (arr[j] > arr[j + 1]) {
                    swap(arr, j, j + 1);
                    noSwaps = false;
                }
            }
            if (noSwaps)
                break;
        }
        return arr;
    }
}
