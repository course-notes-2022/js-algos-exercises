| i   | curr | j   | arr[j] | arr[j] > curr? | j+1 | arr[j+1] | arr (in `j`)    | j(in `i`) | j+1 | arr[j+1] | arr              |
| --- | ---- | --- | ------ | -------------- | --- | -------- | --------------- | --------- | --- | -------- | ---------------- |
| 1   | 19   | 0   | 29     | true           | 1   | 29       | [29,29,-9,99,0] | -1        | 0   | 19       | [19,29,-9,99,0]  |
| 2   | 19   | 1   | 29     | true           | 2   | 29       | [19,29,29,99,0] |           |     |          | [19,29,29,99,0]  |
| 2   | 19   | 0   | 19     | true           | 1   | 29       |                 |           |     |          | [19,]            |
| 3   | 99   | 2   | 19     | false          | 3   | 19       |                 |           |     |          | [29,-9,19,99,0]  |
| 3   | 99   | 1   | -9     | false          | 2   | 19       |                 |           |     |          | [29,-9,19,99,0]  |
| 3   | 99   | 0   | 29     | false          | 1   | -9       |                 |           |     |          | [29,-9,19,99,0]  |
| 4   | 0    | 3   | 99     | true           | 4   | 99       |                 |           |     |          | [29,-9,19,99,99] |
| 4   | 0    | 2   | 19     | true           | 3   | 19       |                 |           |     |          | [29,-9,19,19,99] |
| 4   | 0    | 1   | -9     | false          | 2   | 19       |                 |           |     |          | [29,-9,19,19,99] |
| 4   | 0    | 0   | 29     | true           | 1   | 0        |                 |           |     |          | [29,0,19,19,99]  |
