# Insertion Sort

![insertion sort](./gifs/insertion-sort.gif)

## Instructor Solution

```js
function insertionSort(arr) {
  for (var i = 1; i < arr.length; i++) {
    var curr = arr[i];
    for (var j = i - 1; j >= 0 && arr[j] > curr; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = curr;
  }
  return arr;
}

//           0   1    2  3   4
const arr = [29, 19, -9, 99, 0];

console.log(insertionSort(arr));
```

## Improved: Allows use of `let`

```js
function insertionSort2(arr) {
  for (let i = 1; i < arr.length; i++) {
    let currentVal = arr[i];

    let j;

    for (j = i - 1; j >= 0 && arr[j] > currentVal; j--) {
      arr[j + 1] = arr[j];
    }
    arr[j + 1] = currentVal;
  }
  return arr;
}
```

| `i` | arr[i] | `curr` | `j` | arr[j] | j >= 0 && arr[j] > curr? | j+1 | arr[j+1] | arr              | `j`-- |
| --- | ------ | ------ | --- | ------ | ------------------------ | --- | -------- | ---------------- | ----- |
| 1   | 19     | 19     | 0   | 29     | true && true             | 1   | 29       | [19,29,-9,99,0]  | -1    |
| 2   | -9     | -9     | 1   | 29     | true && true             | 2   | 29       | [19,29,29,99,0]  | 0     |
| 2   | 29     | -9     | 0   | 19     | true && true             | 1   | 19       | [-9,19,29,99,0]  | -1    |
| 3   | 99     | 99     | 2   | 29     | true && false            | -   | -        | [-9,19,29,99,0]  | 1     |
| 3   | 99     | 99     | 1   | 19     | true && false            | -   | -        | [-9,19,29,99,0]  | 0     |
| 3   | 99     | 99     | 0   | -9     | true && false            | -   | -        | [-9,19,29,99,0]  | -1    |
| 4   | 0      | 0      | 3   | 99     | true && true             | 4   | 99       | [-9,19,29,99,99] | 2     |
| 4   | 99     | 0      | 2   | 29     | true && true             | 3   | 29       | [-9,19,29,29,99] | 1     |
| 4   | 99     | 0      | 1   | 19     | true && true             | 2   | 19       | [-9,19,19,29,99] | 0     |
| 4   | 99     | 0      | 0   | -9     | true && false            | -   | -        | [-9,0,19,29,99]  | -1    |
