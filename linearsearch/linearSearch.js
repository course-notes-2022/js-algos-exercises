/*
Linear search focuses on searching **arrays**.

## How do we Search an Array?

The **simplest** way would be to start at the beginning, and check **each
element** to see if it is the one we want. This method is called **linear
search**.
*/

// MY Solution
function linearSearch(arr, value) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] == value) return i;
  }
  return -1;
}

console.log(linearSearch([5, 8, 1, 100, 12, 3, 12], 12));

// INSTRUCTOR Solution
function linearSearchInstructor(arr, val) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === val) return i;
  }
  return -1;
}
