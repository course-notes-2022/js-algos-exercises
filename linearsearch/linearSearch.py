def linearSearch(myList, value):
    for i in range(len(myList)):
        if myList[i] == value:
            return i
    return -1

print(linearSearch([1,3,5,7,999,10,20], 999)) # 4
print(linearSearch([9,8,7,6,5], -12)) # -1