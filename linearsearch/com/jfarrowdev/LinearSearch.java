package linearsearch.com.jfarrowdev;

public class LinearSearch {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5, 77 };
        System.out.println(linearSearch(arr, 2)); //
        System.out.println(linearSearch(arr, -1000));
    }

    public static int linearSearch(int[] arr, int val) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == val) {
                return i;
            }
        }
        return -1;
    }
}