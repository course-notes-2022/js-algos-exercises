# Selection Sort

![selection sort](./gifs/selection-sort.gif)

```js
function swap(arr, idx1, idx2) {
  let temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let lowest = i;
    for (var j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[lowest]) {
        lowest = j;
      }
    }
    swap(arr, i, lowest);
  }
  return arr;
}

console.log(selectionSort([11, 999, -12, 0]));
```

| `i` | `lowest` | `j` | arr[j] | arr[lowest] | arr[j] < arr[lowest]? | `lowest` | arr               |
| --- | -------- | --- | ------ | ----------- | --------------------- | -------- | ----------------- |
| 0   | 0        | 1   | 999    | 11          | false                 | 0        | [11, 999, -12, 0] |
| 0   | 0        | 2   | -12    | 11          | true                  | 2        | [11, 999, -12, 0] |
| 0   | 2        | 3   | 0      | -12         | false                 | 2        | [-12, 999, 11, 0] |
| 1   | 1        | 2   | 11     | 999         | true                  | 1        | [-12, 999, 11, 0] |
| 1   | 1        | 3   | 0      | 999         | true                  | 3        | [-12, 0, 999, 11] |
| 2   | 2        | 3   | 11     | 999         | true                  | 3        | [-12, 0, 11, 999] |

## Explanation

In **Selection Sort**, we start by looping over the entire array. With **each
iteration**, select the `i`-th index of the array as the index of the **lowest**
element:

```js
for (let i = 0; i < arr.length; i++) {
  let lowest = i;
}
```

In a **nested** loop, we then compare `arr[lowest]` (which is the **same** as
`arr[i]`) to `arr[i+1]` (which is the **same** as `arr[j]`). If arr[j] is **less
than** arr[lowest], `j` becomes the new `lowest` index:

```js
for (let i = 0; i < arr.length; i++) {
  let lowest = i;
  for (let j = i + 1; j < arr.length; j++) {
    if (arr[j] < arr[lowest]) {
      lowest = j;
    }
  }
}
```

Finally, at the end of each **outer** loop iteration, we **swap** the positions
of arr[i] and arr[lowest]:

```js
for (let i = 0; i < arr.length; i++) {
  let lowest = i;
  for (let j = i + 1; j < arr.length; j++) {
    if (arr[j] < arr[lowest]) {
      lowest = j;
    }
  }
  swap(arr, i, lowest);
}

return arr;
```
