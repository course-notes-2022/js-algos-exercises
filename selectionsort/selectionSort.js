/*
- Store the first element as the smallest value you've seen so far.
- Compare this item to the next item in the array until you find a smaller
  number.
- If a smaller number is found, designate that smaller number's INDEX to be the
  new "minimum" and continue until the end of the array.
- If the minimum is not the value (index) you initially began with, swap the two
  values.
- Repeat this with the next element until the array is sorted.
- **Note**: You **will** need two loops for this implementation.
*/
function swap(arr, idx1, idx2) {
  let temp = arr[idx1];
  arr[idx1] = arr[idx2];
  arr[idx2] = temp;
}

function selectionSort(arr) {
  for (let i = 0; i < arr.length; i++) {
    let lowest = i;
    for (var j = i + 1; j < arr.length; j++) {
      if (arr[j] < arr[lowest]) {
        lowest = j;
      }
    }
    swap(arr, i, lowest);
  }
  return arr;
}

console.log(selectionSort([99, 1480, -12, 0, 12, -208, 999], 1));
console.log(selectionSort([11, 999, -12, 0]));
