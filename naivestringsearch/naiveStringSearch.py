# - Define a function called `stringSearch`. It takes a string to search, and the
#   pattern to search for.
# - Loop over the longer string.
# - If the characters don't match, break out of the inner loop
# - If the characters do match, keep going (i.e. go to the next "i")
# - If you complete the inner loop and find a match, increment the count of
#   matches
# - Return the count

def stringSearch(str, pattern):
    matchCount = 0

    for i in range(0, len(str)):
        for j in range(0, len(pattern)):
            if str[i] != pattern[j]:
                break
            else:
                i+= 1
                if j == len(pattern) - 1:
                    matchCount+= 1

    return matchCount


print(stringSearch("omgwowomgz", "omg"))
print(stringSearch("fobarbazfquuzfoozaaq", "foo"))
