/*
- Define a function called `stringSearch`. It takes a string to search, and the
  pattern to search for.
- Loop over the longer string.
- If the characters don't match, break out of the inner loop
- If the characters do match, keep going (i.e. go to the next "i")
- If you complete the inner loop and find a match, increment the count of
  matches
- Return the count
*/

function stringSearch(str, pattern) {
  let matchCount = 0;
  for (let i = 0; i < str.length; i++) {
    for (let j = 0; j < pattern.length; j++) {
      if (str[i] !== pattern[j]) {
        break;
      }
      if (str[i] == pattern[j]) {
        i += 1;
        if (j === pattern.length - 1) {
          matchCount += 1;
        }
      }
    }
  }
  return matchCount;
}

console.log(stringSearch('omgwowomgz', 'omg'));
