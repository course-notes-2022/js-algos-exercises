package naivestringsearch.com.jfarrowdev;

public class NaiveStringSearch {
    
    public static void main(String[] args) {
        System.out.println(stringSearch("omgwowomgz", "omg"));
    }

    public static int stringSearch(String str, String pattern) {
        int matchCount = 0;

        for (int i = 0; i < str.length(); i += 1) {
            for (int j = 0; j < pattern.length(); j += 1) {
                if (str.charAt(i) != pattern.charAt(j)) {
                    break;
                } else {
                    i += 1;
                    if (j == pattern.length() - 1) {
                        matchCount += 1;
                    }
                }
            }
        }
        return matchCount;
    }
}
