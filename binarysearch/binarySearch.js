/*
Binary Search
*/

function binarySearch(sortedArr, value) {
  let left = 0;
  let right = sortedArr.length - 1;

  while (left <= right) {
    let middle = Math.floor((left + right) / 2);

    if (arr[middle] === value) return middle;

    if (value > arr[middle]) {
      left = middle + 1;
    } else if (value < arr[middle]) {
      right = middle - 1;
    }
  }
  return -1;
}

//           0  1  2  3  4  5   6   7
const arr = [1, 3, 5, 7, 9, 11, 13, 15];

console.log(binarySearch(arr, 8)); // -1
console.log(binarySearch(arr, 3)); // 1
// console.log(binarySearch(arr, 7)); // 3
// console.log(binarySearch(arr, 15)); // 7
