package binarysearch.com.jfarrowdev;

import java.lang.Math;

public class BinarySearch {
    public static void main(String[] args) {
        int[] arr = { 1, 2, 3, 4, 5, 6 };

        System.out.println(binarySearch(arr, 6));
        System.out.println(binarySearch(arr, 1));
        System.out.println(binarySearch(arr, 3));
        System.out.println(binarySearch(arr, -99));
    }

    public static int binarySearch(int[] arr, int val) {
        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int middle = (int) Math.floor((left + right) / 2);

            if (arr[middle] == val)
                return middle;
            if (arr[middle] > val) {
                right = middle - 1;
            } else if (arr[middle] < val) {
                left = middle + 1;
            }
        }
        
        return -1;
    }
}
