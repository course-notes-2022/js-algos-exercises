# Binary Search

**Binary Search** is a much faster form of search. Rather than eliminating one
element at a time, you can eliminate **half** of the elements. However the array
**must** be sorted!

## Pseudocode

1. Pick the **halfway point** in the sorted array.
2. Compare your search term with the element at the halfway point index.
3. `search term > index`?
4. If yes, pick the **halfway point** of the **second half** of the list. If no,
   pick the **halfway point** of the **first half** of the list.
5. Repeat steps 2 - 4 until you have found your search term!

## My Implementation

```js
function binarySearch(sortedArr, value) {
  let left = 0;
  let right = sortedArr.length - 1;

  while (left <= right) {
    let middle = Math.floor((left + right) / 2);

    if (arr[middle] === value) return middle;

    if (value > arr[middle]) {
      left = middle + 1;
    } else if (value < arr[middle]) {
      right = middle - 1;
    }
  }
  return -1;
}
```

## Explanation

In **binary search** we start with a **sorted** array. The array MUST be sorted
for binary search to work.

We separate the array into **halves** based on its length. Find the **middle
index** of the array. We then compare the `value` to the item at `arr[middle]`:

> Is arr[middle] GREATER than value? If YES, we can elminate all values in the
> array <= arr[middle]! Is arr[middle] LESS THAN value? If YES, we can eliminate
> all values in the array >= arr[middle]!
