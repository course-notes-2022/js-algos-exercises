from math import floor

def binarySearch(sorted_arr, val):
    left = 0
    right = len(sorted_arr) - 1
    
    while left <= right:
        middle = floor((left + right) / 2)
        
        if(arr[middle] == val):
            return middle
        if arr[middle] > val:
            right = middle -1
        if arr[middle] < val:
            left = middle + 1

    return -1

#      0 1 2 3 4 5
arr = [1,2,3,4,5,6]

print(binarySearch(arr, 12))
print(binarySearch(arr, 4))
print(binarySearch(arr, 1))
print(binarySearch(arr, 6))
